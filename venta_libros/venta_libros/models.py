from django.db import models


class Users(models.Model):
    user_id = models.BigAutoField(primary_key=True)
    email = models.CharField(max_length=255)
    password = models.CharField(max_length=50)
    name = models.CharField(max_length=255)
    address = models.CharField(max_length=500)
    phone = models.CharField(max_length=50)
    class Meta:
        db_table = "usuarios"

class Books(models.Model):
    id_book = models.BigAutoField(primary_key=True)
    nombre = models.CharField(max_length=255)
    precio = models.FloatField()
    autor = models.CharField(max_length=500)
    editorial = models.CharField(max_length=255)
    class Meta:
        db_table = "libros"
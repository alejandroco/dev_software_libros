from django.shortcuts import redirect, render
from venta_libros.models import Users, Books

# Get

def display_inicio_sesion(request):
    if request.method == "GET":
        return render(request, "index.html")

def display_libros(request):
    books = Books.objects.all()
    return render(request, "display_libros.html", {"books": books})

def display_carrito(request):
    return render(request, "carrito.html")

def display_pago(request):
    return render(request, "pago.html")

def display_historial(request):
    return render(request, "historial.html")

def display_perfil(request):
    data = Users.objects.get(user_id=1)
    return render(request, "perfil.html", {"user": data})

# Post

def inicio_sesion(request):
    if request.method == "POST":
        email = request.POST["email"]
        password = request.POST["password"]
        data = Users.objects.all()
        for i in data:
            if email == i.email and password == i.password:
                return redirect("/seleccion-libros")
        return redirect("/")